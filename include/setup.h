#ifndef __T_SETUP_H__
#define __T_SETUP_H__

#include <allegro5/allegro.h>
#include "dict.h"

#define WIDTH 480
#define HEIGHT 512

/*
 * Globals
 */ 
ALLEGRO_DISPLAY *display;
ALLEGRO_EVENT_QUEUE *event_queue;
ALLEGRO_TIMER *timer;

tDict *samples;

bool ti_setup();
void ti_cleanup();

/*
typedef struct {

    ALLEGRO_DISPLAY *display;
    ALLEGRO_EVENT_QUEUE *event_queue;
    ALLEGRO_TIMER *timer;
    int width;
    int height;
    tDict *fonts;
    tDict *samples;
} tGameData;
*/

#endif

