#ifndef __T_DRAW_H__
#define __T_DRAW_H__

#include "board.h"

void ti_draw_block(tBlock *block);
void ti_draw_board(tBoard *board);
void ti_draw_next_tetromino(tBoard *board);

#endif

