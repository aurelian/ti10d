#ifndef __T_CALLBACKS_H__
#define __T_CALLBACKS_H__

void onRowComplete();
void on4RowsComplete();

void onMoveLeft();
void onMoveRight();
void onRotate();
void onDrop();
void onAccelerate();

#endif

