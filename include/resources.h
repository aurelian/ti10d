#ifndef __T_RESOURCES_H__
#define __T_RESOURCES_H__

ALLEGRO_FONT *ti_load_font(int size, const char *font_name);
ALLEGRO_BITMAP *ti_load_bitmap(const char *filename);
tDict *ti_load_samples();

#endif
