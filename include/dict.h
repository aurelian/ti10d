#ifndef __T_DICT_H__
#define __T_DICT_H__

/*
 * This looks like a key - value structure
 */
typedef struct {

    int length; /* #elements */
    char **keys;
    void **vals;
} tDict;

tDict *ti_dict_init();
void *ti_dict_get(tDict *dict, const char *key);
void ti_dict_set(tDict *dict, const char *key, void *val);
void ti_dict_destroy(tDict *dict);

#endif
