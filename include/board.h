#ifndef __T_BOARD_H__
#define __T_BOARD_H__

#define BLOCK_SIZE 16

/*
 * types
 */ 
typedef struct
{
  int x;
  int y;
  int color[4];
  
} tBlock;

typedef struct
{
  tBlock *tetromino[4];
  tBlock ***blocks;

  int rows;
  int cols;  
  int current_tetromino;
  int current_rotation;
  int next_tetromino;
  int next_rotation;
  int score;
  int level;
  int lines;

} tBoard;

tBoard * ti_init_board(int rows, int cols);
void ti_destroy_board(tBoard *board);
bool ti_update_board(tBoard *board);

bool ti_add_tetromino(tBoard *board);
bool ti_drop_tetromino(tBoard *board);
bool ti_move_tetromino_left(tBoard *board);
bool ti_move_tetromino_right(tBoard *board);
bool ti_rotate_tetromino(tBoard *board);
bool ti_accelerate_tetromino(tBoard *board);

/*
 * callbacks
 */ 
extern void onRowComplete();
extern void on4RowsComplete();
extern void onMoveLeft();
extern void onMoveRight();
extern void onRotate();
extern void onDrop();
extern void onAccelerate();

#endif
