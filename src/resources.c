#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "dict.h"
#include "resources.h"

const char *ti_get_resource_path(const char *base_path, const char *filename) {

    static ALLEGRO_PATH *resource;
    static ALLEGRO_PATH *path;

    char buffer[strlen(base_path)+strlen(filename)+1];
    snprintf(buffer, sizeof(buffer), "%s%s", base_path, filename);

    if(!path)
        path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);

    if(resource)
        al_destroy_path(resource);

    resource = al_create_path(buffer);
    al_rebase_path(path, resource);

    return al_path_cstr(resource, ALLEGRO_NATIVE_PATH_SEP);
}

ALLEGRO_FONT *ti_load_font(int size, const char *font_name) {

    const char *font_path;
    ALLEGRO_FONT *font;

    font_path = ti_get_resource_path("data/fonts/", font_name);
    font      = al_load_font(font_path, size, 0);
    assert(font);

    return font;
}

ALLEGRO_BITMAP *ti_load_bitmap(const char *bitmap_name) {

    const char *bitmap_path;
    ALLEGRO_BITMAP *bitmap;

    bitmap_path = ti_get_resource_path("data/", bitmap_name);
    bitmap      = al_load_bitmap(bitmap_path);
    assert(bitmap);

    return bitmap;
}

tDict *ti_load_samples() {

    ALLEGRO_PATH *sounds_path;
    ALLEGRO_PATH *current_path;
    ALLEGRO_PATH *base_path;
    ALLEGRO_FS_ENTRY *base_entry;
    ALLEGRO_FS_ENTRY *current_entry;
    tDict *_samples;

    _samples = ti_dict_init();

    sounds_path = al_create_path("data/sounds");
    base_path   = al_get_standard_path(ALLEGRO_RESOURCES_PATH);

    /* TODO: this returns false on error */
    al_rebase_path(base_path, sounds_path);

    base_entry = al_create_fs_entry( al_path_cstr(sounds_path, ALLEGRO_NATIVE_PATH_SEP) );

    al_open_directory(base_entry);

    while((current_entry= al_read_directory(base_entry))) {
        if(al_get_fs_entry_mode(current_entry) & ALLEGRO_FILEMODE_ISFILE) {
            current_path = al_create_path(al_get_fs_entry_name(current_entry));

            ti_dict_set(_samples,
                    al_get_path_filename(current_path),
                    al_load_sample(al_get_fs_entry_name(current_entry))
            );

            al_destroy_path(current_path);
        }
        al_destroy_fs_entry(current_entry);
    }

    al_destroy_fs_entry(base_entry);
    al_destroy_path(base_path);
    al_destroy_path(sounds_path);

    al_reserve_samples(_samples->length);

    return _samples;
}
