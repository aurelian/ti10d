#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "dict.h"
#include "setup.h"
#include "callbacks.h"

void onRowComplete()
{
    ALLEGRO_SAMPLE *sample;

    if((sample = ti_dict_get(samples, "line.wav")))
        al_play_sample(sample, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void on4RowsComplete()
{
    ALLEGRO_SAMPLE *sample;
    char key[13];

    snprintf(key, sizeof(key), "4lines-%d.wav", rand() % 2);

    if((sample = ti_dict_get(samples, key)))
        al_play_sample(sample, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);

}

void onMoveLeft()
{
    ALLEGRO_SAMPLE *sample;

    if((sample = ti_dict_get(samples, "move.wav")))
        al_play_sample(sample, 1, -1, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void onMoveRight()
{
    ALLEGRO_SAMPLE *sample;

    if((sample = ti_dict_get(samples, "move.wav")))
        al_play_sample(sample, 1, 1, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void onRotate()
{
    ALLEGRO_SAMPLE *sample;

    if((sample = ti_dict_get(samples, "rotate.wav")))
        al_play_sample(sample, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void onDrop()
{
    ALLEGRO_SAMPLE *sample;

    if((sample = ti_dict_get(samples, "drop.wav")))
        al_play_sample(sample, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void onAccelerate()
{

}

