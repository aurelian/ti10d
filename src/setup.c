#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "setup.h"
#include "dict.h"
#include "resources.h"

bool ti_setup() {
    display = NULL;
    event_queue = NULL;
    timer = NULL;

    const float FPS = 22;

    if(!al_init()) {
        printf("failed to initialize allegro!\n");
        return 0;
    }

      /* TODO: try this out
       * http://stackoverflow.com/questions/6391423/anti-aliasing-in-allegro-5/6392383#6392383*/
    al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
    al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);

    al_install_keyboard();
    al_install_audio();

    al_init_primitives_addon();
    al_init_image_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_acodec_addon();

    display = al_create_display(WIDTH, HEIGHT);
    if(!display) {
        printf("failed to create display!\n");
        return false;
    }

    event_queue = al_create_event_queue();
    if(!event_queue) {
        printf("failed to create event queue!\n");
        return false;
    }

    timer = al_create_timer(1.0 / FPS);

    samples = ti_load_samples();

    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));

    al_clear_to_color(al_map_rgb(224,220,208));
    al_flip_display();

    return true;
}

void ti_cleanup() {

    int i;

    for(i = 0; i < samples->length; i++)
        al_destroy_sample(samples->vals[i]);

    ti_dict_destroy(samples);

    if(timer)
        al_destroy_timer(timer);

    if(event_queue)
        al_destroy_event_queue(event_queue);

    if(display)
        al_destroy_display(display);

}

