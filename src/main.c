#include "setup.h"
#include "game.h"

int main(void)
{
    if(ti_setup()) {
        ti_run();
    }

    ti_cleanup();

    return 0;
}

