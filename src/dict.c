#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "dict.h"

tDict *ti_dict_init() {
    
    tDict *dict = malloc(sizeof(tDict));
    dict->keys = NULL;
    dict->vals = NULL;
    dict->length = 0;
    return dict;
}

void *ti_dict_get(tDict *dict, const char *key) {
    
    int i;

    for (i = 0; i < dict->length; i++)
        if (!strcmp(dict->keys[i], key))
            return dict->vals[i];

    return NULL;
}

void ti_dict_set(tDict *dict, const char *key, void *val) {
    
    if (dict->length == 0) {
        dict->keys = malloc(sizeof(char *));
        assert(dict->keys);
        dict->vals = malloc(sizeof(void *));
        assert(dict->vals);
    } else {
        void * tmp_keys = realloc(dict->keys, sizeof(char *) * (dict->length + 1));
        assert(tmp_keys);
        dict->keys = tmp_keys;

        void * tmp_vals = realloc(dict->vals, sizeof(void *) * (dict->length + 1));
        assert(tmp_vals);
        dict->vals = tmp_vals;
    }

    dict->keys[dict->length] = strdup(key);
    dict->vals[dict->length] = val;
    dict->length++;
}

void ti_dict_destroy(tDict *dict) {

    int i;
    
    for (i = 0; i < dict->length; i++)
        free(dict->keys[i]);
    
    free(dict->keys);
    free(dict->vals);
    free(dict);
}
