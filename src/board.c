#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "board.h"
#include "tetromino.h"

#ifdef DEBUG
void ti_print_board(tBoard *board) {

    int i, j;
    printf("##########################\n");

    for(i=0; i<board->rows;i++){
        printf("#");
        for(j=0;j<board->cols;j++){
            if(board->blocks[j][i] == NULL)
                printf(" 0 ");
            else
                /* printf("\033[1;32m %i,%i \033[0m", j, i); */
                printf("\033[1;32m 1 \033[0m");
                /* printf("\033[1;32m %p \033[0m", board->blocks[j][i]); */
        }
        printf("#\n");
    }

    printf("##########################\n");

}
#endif

/*
 * it adds a block on the board at x / y position.
 */
tBlock *ti_add_block(tBoard *board, int x, int y) {

    tBlock *block;

    block= malloc(sizeof(tBlock));
    assert(block);

    block->x = x;
    block->y = y;

    block->color[0]= tetromino_colors[board->current_tetromino][0];
    block->color[1]= tetromino_colors[board->current_tetromino][1];
    block->color[2]= tetromino_colors[board->current_tetromino][2];
    block->color[3]= tetromino_colors[board->current_tetromino][3];
#ifdef DEBUG
    printf("++> %p=> Block(%i, %i)\n", block, block->x, block->y);
#endif
    return block;
}

void ti_destroy_block(tBoard *board, tBlock *block) {
#ifdef DEBUG
    printf( "--> %p=> Block(%i, %i)\n", block, block->x, block->y );
#endif
    board->blocks[block->x][block->y] = NULL;
    free(block);
    block = NULL;
}

/*
 * obviosly, it adds a tetromino on the board, if a block fails to be added, it returns false #=> game over!
 */
bool ti_add_tetromino(tBoard *board) {

    int i, j;
    int k = 0;

    board->current_tetromino= board->next_tetromino;
    board->current_rotation = board->next_rotation;
    board->next_tetromino = rand() % 7;
    board->next_rotation  = rand() % 4;

    for( i = 0; i < 4; i++ )
        for( j = 0; j < 4; j++ )
            if(tetrominoes[board->current_tetromino][board->current_rotation][i][j] == 1 && board->blocks[j+(board->cols/2)-1][i] == NULL)
                board->tetromino[k++]= ti_add_block(board, j+(board->cols/2)-1, i);

    return k == 4;
}

/*
 * Checks if row is full, returns false if is not
 */
bool ti_complete_row(tBoard *board, int row) {

    int x;

    for(x = 0; x < board->cols; x++)
        if(board->blocks[x][row] == NULL)
            return false;

    return true;
}

/*
 * Removes row from the board
 */
void ti_remove_row(tBoard *board, int row) {

    int x;

    for(x = 0; x < board->cols; x++)
        ti_destroy_block(board, board->blocks[x][row]);

    onRowComplete();
}

/*
 * Calculates the distance till block will hit ground (or other locked blocks)
 */
int ti_drop_distance(tBoard *board, tBlock *block) {

    int i;
    int dist = 0;

    for(i = block->y + 1; i < board->rows; i++)
        if(board->blocks[block->x][i] != NULL)
            break;
        else
            dist++;

    return dist;
}

/*
 * Drops all the blocks above given row
 */
void ti_drop_blocks(tBoard *board, int row) {

    int x, y;
    tBlock *block;

    for(y = row - 1; y >= 0; y--) {
        for(x = 0; x < board->cols; x++) {
            if(board->blocks[x][y] != NULL) {
                block= board->blocks[x][y];
                block->y++;
                board->blocks[x][y+1]= block;
                board->blocks[x][y]= NULL;
            }
        }
    }
}

/*
 * Updates board statistics: score, lines and level
 */
void ti_update_stats(tBoard *board, int rows) {

    int score;

    switch(rows) {
        case 1:
            score = 40 * (board->level + 1);
            break;
        case 2:
            score = 100 * (board->level + 1);
            break;
        case 3:
            score = 300 * (board->level + 1);
            break;
        case 4:
            score = 1200 * (board->level + 1);
            on4RowsComplete();
            break;
        default:
            score = 0;
            break;
    }

    board->lines += rows;
    board->score += score;

    board->level = board->lines/10;

}

void ti_check_complete_rows(tBoard *board) {

    int i;
    int row;
    int completed_rows = 0;

    /* TODO:
     * this can be optimized to skip already checked rows, e.g. an O will check for 4 rows when only 2 will do
     */
    for(i = 0; i < 4; i++) {
        row= board->tetromino[i]->y;

        if(ti_complete_row(board, row)) {
#ifdef DEBUG            
            printf("--> removing row %i\n", row);
#endif
            ti_remove_row(board, row);
            /* potential problem... */
            ti_drop_blocks(board, row);
            completed_rows++;
        }
    }

    ti_update_stats(board, completed_rows);

}

/*
 * nothing fancy, this populates the board->blocks and spawns next tetromino
 */
bool ti_lock_tetromino(tBoard *board) {

    int i;

    for(i = 0; i < 4; i++)
        board->blocks[board->tetromino[i]->x][board->tetromino[i]->y]= board->tetromino[i];

    ti_check_complete_rows(board);

    return ti_add_tetromino(board);
}

bool ti_move_tetromino_left(tBoard *board) {

    int i;

    for(i = 0; i < 4; i++)
        if( board->tetromino[i]->x - 1 < 0 || board->blocks[board->tetromino[i]->x-1][board->tetromino[i]->y] != NULL)
            return false;

    /* actually: perform the move */
    for(i = 0; i < 4; i++)
        board->tetromino[i]->x--;

    onMoveLeft();

    return true;
}

bool ti_move_tetromino_right(tBoard *board) {

    int i;

    for(i = 0; i < 4; i++)
        if( board->tetromino[i]->x + 1 >= board->cols || board->blocks[board->tetromino[i]->x+1][board->tetromino[i]->y] != NULL)
            return false;

    /* actually: perform the move */
    for(i = 0; i < 4; i++)
        board->tetromino[i]->x++;

    onMoveRight();

    return true;
}

/*
 * Essentially, this is same as ti_update_board with the exception that it won't lock the tetromino, nor spawn another one if blocked
 */
bool ti_accelerate_tetromino(tBoard *board) {

    int i;

    for(i = 0; i < 4; i++)
        if( board->tetromino[i]->y + 1 >= board->rows || board->blocks[board->tetromino[i]->x][board->tetromino[i]->y+1] != NULL )
            return false;

    /* actually: perform the move */
    for(i = 0; i < 4; i++)
        board->tetromino[i]->y++;

    onAccelerate();

    board->score++;

    return true;
}


bool ti_drop_tetromino(tBoard *board) {

    int i, min, dist;

    min= board->rows;

    for(i = 0; i < 4; i++) {
        dist= ti_drop_distance(board, board->tetromino[i]);
        if(dist < min)
            min = dist;
    }

    for(i = 0; i < 4; i++)
        board->tetromino[i]->y += min;

    /* increse the score by falling distance */
    board->score += min;

    onDrop();

    return ti_lock_tetromino(board);

}

/*
 * Rotates current tetromino around a pivot block (board->tetromino[1])
 *
 * returns true if the rotation was completed false if the rotation is not possible
 *
 * TODO: implement wall-kick: pivot point should be diplaced by distance that will allow the move and then the rotation should be recalculated
 */
bool ti_rotate_tetromino(tBoard *board) {

    if(board->current_tetromino == 6) /* no need to rotate O shape */
        return true;

    int i;

    int x, y; /* old block position */
    int xx, yy; /* new block position */

    int rotations[4][2];

    tBlock *block;

    for(i = 0; i < 4; i++) {
        block = board->tetromino[i];
        x = block->x;
        y = block->y;

        xx = board->tetromino[1]->x + board->tetromino[1]->y - y;
        yy = board->tetromino[1]->y - board->tetromino[1]->x + x;

        if(xx <= 0 || yy <= 0 || xx >= board->cols || yy >= board->rows || board->blocks[xx][yy] != NULL)
            return false;

        rotations[i][0]= xx;
        rotations[i][1]= yy;
    }

    for(i = 0; i < 4; i++) {
        board->tetromino[i]->x = rotations[i][0];
        board->tetromino[i]->y = rotations[i][1];
    }

    onRotate();

    return true;

}

tBoard * ti_init_board(int rows, int cols) {

    int i, j;
    tBoard *board;

    board= malloc(sizeof(tBoard));
    assert(board);

    board->rows= rows;
    board->cols= cols;

    board->next_tetromino= rand() % 7;
    board->next_rotation = rand() % 4;

    board->score= 0;
    board->level= 0;
    board->lines= 0;

    board->blocks= malloc(sizeof(tBlock *) * cols);
    assert(board->blocks);

    board->blocks[0]= malloc(rows * cols * sizeof(tBlock));
    assert(board->blocks[0]);

    for(i = 1; i < cols; i++)
        board->blocks[i]= board->blocks[0] + i * rows;

    /* init all blocks to null pointer (saw strange things happening without this) */
    for(i = 0; i < board->rows; i++)
        for(j = 0; j < board->cols; j++)
            board->blocks[j][i] = NULL;

    return board;

}

void ti_destroy_board(tBoard *board) {

    int i, j;

    /* this is kinda lame: it locks current tetromino so it can be freed next. */
    for(i = 0; i < 4; i++)
        board->blocks[board->tetromino[i]->x][board->tetromino[i]->y]= board->tetromino[i];

    for(i = 0; i < board->rows; i++)
        for(j = 0; j < board->cols; j++)
            if(board->blocks[j][i] != NULL)
                ti_destroy_block(board, board->blocks[j][i]);

    free(board->blocks);
    free(board);
}

bool ti_update_board(tBoard *board) {

    int i;

    /* this will figure out if the group can be moved down by 1 unit
     * it returns quickly if any of the tetromino blocks can't be moved further
     */
    for(i = 0; i < 4; i++)
        if( board->tetromino[i]->y + 1 >= board->rows || board->blocks[board->tetromino[i]->x][board->tetromino[i]->y+1] != NULL )
            return ti_lock_tetromino(board);

    /* actually: perform the move */
    for(i = 0; i < 4; i++)
        board->tetromino[i]->y++;

    return true;

}
