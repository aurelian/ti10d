#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "draw.h"
#include "tetromino.h"

/* {{{ Drawing routines */

void ti_draw_block(tBlock *block) {

    al_draw_filled_rectangle(
            block->x * BLOCK_SIZE + 0.5 + 32,
            block->y * BLOCK_SIZE + 0.5 + 32,
            block->x * BLOCK_SIZE - 0.5 + BLOCK_SIZE + 32,
            block->y * BLOCK_SIZE - 0.5 + BLOCK_SIZE + 32,
            al_map_rgba( block->color[0], block->color[1], block->color[2], block->color[3] )
    );
}

void ti_draw_board(tBoard *board) {

    int i, j;

    for(i = 0; i < board->rows; i++) {
        for(j = 0; j < board->cols; j++) {
            if(board->blocks[j][i] != NULL) {
                ti_draw_block(board->blocks[j][i]);
            }
        }
    }

    /* current tetromino */
    for(i=0; i < 4; i++)
        ti_draw_block(board->tetromino[i]);

    al_draw_rectangle( 32, 32, board->cols * BLOCK_SIZE + 32, board->rows * BLOCK_SIZE + 32, al_map_rgb(0, 0, 0), 1 );
}

void ti_draw_next_tetromino(tBoard *board) {

    int i, j;

    int x_offset = (board->cols + 3) * BLOCK_SIZE;
    int y_offset = BLOCK_SIZE * 3;

    for(i = 0; i < 4; i++) {
        for(j = 0; j < 4; j++) {

            if(tetrominoes[board->next_tetromino][board->next_rotation][i][j] == 1) {
                al_draw_filled_rectangle(
                    x_offset + j * BLOCK_SIZE + 0.5 + 32,
                    y_offset + i * BLOCK_SIZE + 0.5 + 32,
                    x_offset + j * BLOCK_SIZE - 0.5 + BLOCK_SIZE + 32,
                    y_offset + i * BLOCK_SIZE - 0.5 + BLOCK_SIZE + 32,

                    al_map_rgb(
                        tetromino_colors[board->next_tetromino][0],
                        tetromino_colors[board->next_tetromino][1],
                        tetromino_colors[board->next_tetromino][2]
                    )
                );
            }
        }
    }
}

/* }}} */

