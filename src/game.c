#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include "setup.h"
#include "resources.h"
#include "board.h"
#include "tetromino.h"
#include "draw.h"

#include "game.h"

/*
 * ALLEGRO_USER_DATA_PATH -> ~/Library/ti10d
 * ALLEGRO_USER_SETTINGS_PATH -> ~/Library/Application Support/ti10d/
 */

enum STATE { DONE = 0, INIT = 2, PLAY = 4, OVER = 8 };

void ti_game_init(int *state) {

    ALLEGRO_FONT *font24= ti_load_font(24, "KRONIKA_.ttf");
    ALLEGRO_FONT *font14= ti_load_font(14, "KRONIKA_.ttf");
    ALLEGRO_FONT *font12= ti_load_font(12, "KRONIKA_.ttf");

    ALLEGRO_BITMAP *title = ti_load_bitmap("title.png");

    int i;
    int bbw= al_get_text_width(font24, "Hit a Key to Start");

    const char *text[6]= {"Arrow keys to move left/right", "Up to rotate", "Down to accelerate", "Space to drop", "P for Pause", "ESC to exit"};

    while(*state == INIT) {

        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);

        if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            *state = DONE;
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            *state = (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)? DONE : PLAY;
        }
        else if(ev.type == ALLEGRO_EVENT_TIMER) {

            /*
             * al_draw_line( WIDTH/2, 0, WIDTH/2, HEIGHT, al_map_rgb(255,25,76), 1 );
             * al_draw_line( 0, HEIGHT/2, WIDTH, HEIGHT/2, al_map_rgb(255,25,76), 1 );
             */

            al_draw_bitmap(title, (WIDTH - al_get_bitmap_width(title))/2, 90, 0);

            for(i=0; i < 6; i++)
                al_draw_text(font14, al_map_rgb(254, 25, 72), BLOCK_SIZE*2, 224 + BLOCK_SIZE*i, ALLEGRO_ALIGN_LEFT, text[i]);

            al_draw_text(font12, al_map_rgb(0, 0, 0), 320, 360, ALLEGRO_ALIGN_CENTRE, "handrafted by Aurelian Oancea (c) 2011");

            if(al_get_timer_count(timer) % 32)
                al_draw_text(font24, al_map_rgb(154, 0, 2), (WIDTH-bbw)/2, HEIGHT/2 - BLOCK_SIZE*6,
                             ALLEGRO_ALIGN_LEFT, "Hit a Key to Start");

            al_flip_display();
            al_clear_to_color(al_map_rgb(224,220,208));
        }
    }

    al_destroy_font(font24);
    al_destroy_font(font14);
    al_destroy_font(font12);
}

void ti_game_play(int *state) {

    enum KEYS{UP, DOWN, LEFT, RIGHT, SPACE};
    bool keys[5] = {false, false, false, false, false};

    bool redraw = true;

    bool is_paused = false;

    int64_t last_update = 0;

    int bbw, bbh;

    ALLEGRO_FONT *font16= ti_load_font(16, "kremlin.ttf");
    ALLEGRO_FONT *font64= ti_load_font(64, "kremlin.ttf");

    bbw= al_get_text_width(font64, "paused");
    bbh= 74; /* font size + 10 */

    srand(SEED);

    tBoard *board= ti_init_board(23, 15);

    ti_add_tetromino(board);

    while(*state == PLAY) {

        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);

        if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            *state= DONE;
        }

        else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            switch(ev.keyboard.keycode) {
                case ALLEGRO_KEY_ESCAPE:
                    if(!is_paused)
                        *state = DONE;
                    break;
                case ALLEGRO_KEY_UP:
                    keys[UP] = true;
                    break;
                case ALLEGRO_KEY_DOWN:
                    keys[DOWN] = true;
                    break;
                case ALLEGRO_KEY_RIGHT:
                    keys[RIGHT] = true;
                    break;
                case ALLEGRO_KEY_LEFT:
                    keys[LEFT] = true;
                    break;
                case ALLEGRO_KEY_SPACE:
                    keys[SPACE] = true;
                    break;
                case ALLEGRO_KEY_P:
                    is_paused = is_paused? false : true;
                    break;
            }
        }

        else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
            switch(ev.keyboard.keycode) {
                case ALLEGRO_KEY_UP:
                    keys[UP] = false;
                    break;
                case ALLEGRO_KEY_DOWN:
                    keys[DOWN] = false;
                    break;
                case ALLEGRO_KEY_RIGHT:
                    keys[RIGHT] = false;
                    break;
                case ALLEGRO_KEY_LEFT:
                    keys[LEFT] = false;
                    break;
                case ALLEGRO_KEY_SPACE:
                    keys[SPACE] = false;
                    break;
            }
        }

        else if(ev.type == ALLEGRO_EVENT_TIMER) {
            redraw = true;

            /* if(!is_paused)
                 GameTick( board, keys, timer, &last_update ); */

            if(keys[SPACE] && !is_paused) {
                ti_drop_tetromino(board);
                keys[SPACE]= false;
            }

            if(keys[DOWN] && !is_paused) {
                ti_accelerate_tetromino(board);
            }

            if(keys[LEFT] && !is_paused) {
                ti_move_tetromino_left(board);
            }

            if(keys[RIGHT] && !is_paused) {
                ti_move_tetromino_right(board);
            }

            if(keys[UP] && !is_paused) {
                ti_rotate_tetromino(board);
                keys[UP]= false;
            }

            if(!is_paused && al_get_timer_count(timer) - last_update > 23/(board->level + 1)) {
                if(ti_update_board(board)) {
                    last_update= al_get_timer_count(timer);
                } else {
                    *state = OVER;
                    redraw = false;
                }
            }

        }

        if(redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;

            ti_draw_board(board);

            /* score: */
            al_draw_textf(font16, al_map_rgb(154, 0, 2), (board->cols + 4) * BLOCK_SIZE, 2 * BLOCK_SIZE,
                    ALLEGRO_ALIGN_LEFT, "score: %i", board->score);
            /* level */
            al_draw_textf(font16, al_map_rgb(154, 0, 2), (board->cols + 4) * BLOCK_SIZE, 10 * BLOCK_SIZE,
                    ALLEGRO_ALIGN_LEFT, "level: %i", board->level);

            /* lines */
            al_draw_textf(font16, al_map_rgb(154, 0, 2), (board->cols + 4) * BLOCK_SIZE, 12 * BLOCK_SIZE,
                    ALLEGRO_ALIGN_LEFT, "lines: %i", board->lines);

            ti_draw_next_tetromino(board);

            if(is_paused) {
                al_draw_filled_rounded_rectangle(
                        (HEIGHT - bbw)/2 - BLOCK_SIZE,
                        (WIDTH - bbh)/2 - BLOCK_SIZE,
                        (HEIGHT + bbw)/2 + BLOCK_SIZE,
                        (WIDTH + bbh)/2 +BLOCK_SIZE,
                        BLOCK_SIZE/2,
                        BLOCK_SIZE/2,
                        al_map_rgba(212,202,57, 128)
                    );
                /* blink every 20th frame */
                if(al_get_timer_count(timer) % 20)
                    al_draw_text(font64, al_map_rgb(255,42,14), WIDTH/2 + BLOCK_SIZE, HEIGHT/2 - BLOCK_SIZE*3, ALLEGRO_ALIGN_CENTRE, "paused");
            }

            al_flip_display();
            al_clear_to_color(al_map_rgb(224,220,208));
        }

    } /* end while */

    al_destroy_font(font64);
    al_destroy_font(font16);

    ti_destroy_board(board);

    al_rest(2);
}

void ti_game_over(int *state) {

    ALLEGRO_FONT *font64= ti_load_font(64, "kremlin.ttf");

    /* TODO: if we stayed here more than x seconds, move to INIT */
    while(*state == OVER) {

        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);

        if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            *state = DONE;
        }
        else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            *state = (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)? DONE : PLAY;
        }
        else if(ev.type == ALLEGRO_EVENT_TIMER) {
            al_draw_text(font64, al_map_rgb(154, 0, 2), WIDTH/2 + BLOCK_SIZE, HEIGHT/2 - BLOCK_SIZE*3,
                         ALLEGRO_ALIGN_CENTRE, "game over");
            al_flip_display();
            al_clear_to_color(al_map_rgb(224,220,208));
        }
    }

    al_destroy_font(font64);

}

void ti_run() {

    int state = INIT;

    al_start_timer(timer);

    while(state != DONE) {
        while(state == INIT) {
            ti_game_init(&state);
        }
        while(state == PLAY) {
            ti_game_play(&state);
        }
        while(state == OVER) {
            ti_game_over(&state);
        }
    }

    al_stop_timer(timer);

}

